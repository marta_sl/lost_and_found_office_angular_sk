import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '../../entities/user';
import { UsersService } from '../../services/users.service';
import { UsersPipe } from '../../pipes/users.pipe';

declare var swal: any;
declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css'],
    providers: [UsersService, UsersPipe],
})

export class UsersComponent implements OnInit {
    users: User[];
    errorMessage: String;
    user = new User('', '', '', '', '');

    constructor(
        private usersService: UsersService,
        private router: Router
    ) { }

    getAllUsers() {
        this.usersService.getAllUsers()
            .subscribe(
            users => this.users = users,
            error => this.errorMessage = <any>error
            );
    }

    deleteUser(userDelete: User) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete the user!'
        }).then(() => {
            this.usersService.deleteUser(userDelete.id);
            this.users.splice(this.users.indexOf(userDelete), 1);
            swal(
                'Deleted!',
                'The user has been deleted.',
                'success'
            )
        }, function (dismiss: any) {
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'User will not be deleted',
                    'info'
                )
            }
        })


    }

    submitForm(myForm: NgForm) {
        swal('Good job!',
            'You just added new user!',
            'success');
        let input = JSON.stringify(myForm.value);
        this.usersService.addNewUser(input);
        this.users.push(myForm.value);
        myForm.resetForm();
        jQuery("modal").modal("hide");
        //location.reload();
    }

    ngOnInit(): void {
        this.getAllUsers();
    }


}