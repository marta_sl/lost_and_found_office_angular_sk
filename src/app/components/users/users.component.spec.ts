import { NO_ERRORS_SCHEMA, DebugElement, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { async } from '@angular/core/testing';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { RatingModule } from 'ng2-rating';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule, ConnectionBackend, RequestOptions} from '@angular/http';
import "rxjs/add/observable/of";
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {Router, RouterModule} from "@angular/router";
import { MockBackend } from "@angular/http/testing";


import { User } from '../../entities/user';
import { UsersComponent } from './users.component';
import { UsersService } from '../../services/users.service';
import { UsersPipe } from '../../pipes/users.pipe';

describe('Test Users Component',() =>{
    let comp:    UsersComponent;
    let fixture: ComponentFixture<UsersComponent>;
    let usersService: UsersService;
    let users: User[];
    let spy: any;
    let mockRouter = {
        navigate: jasmine.createSpy('navigate')
      } 


    beforeEach(async(()=>{
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [UsersComponent, UsersPipe],
            providers: [
                { provide: Router},
                { provide: UsersService}],
            
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
                NO_ERRORS_SCHEMA
            ],
        })
        .compileComponents();
    }))

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ FormsModule ],
            declarations: [UsersComponent, UsersPipe],
            providers: [ UsersService, HttpModule, Http, ConnectionBackend,RequestOptions,
                { provide: Router, useValue: mockRouter }, RouterModule],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
                NO_ERRORS_SCHEMA
            ],
        });
        fixture = TestBed.createComponent(UsersComponent);
        comp = fixture.componentInstance;

        usersService = fixture.debugElement.injector.get(UsersService);
        
        spy = spyOn(usersService, 'getAllUsers').and.returnValue(Promise.resolve(users));

    })

    // it('dummy test', () => {
    //     expect(true).toBe(true);
    //   });

    //   it('should not show users before OnInit', () => {
    //     expect(spy.calls.any()).toBe(false, 'users not yet called');
    //   });
})


