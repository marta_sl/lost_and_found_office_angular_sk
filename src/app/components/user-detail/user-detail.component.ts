import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '../../entities/user';
import { LostItem } from '../../entities/lostitem';
import { FoundItem } from '../../entities/founditem';
import { Wishlist } from '../../entities/wishlist';
import { UsersService } from '../../services/users.service';
import { LostItemService } from '../../services/lostitem.service';
import { FoundItemService } from '../../services/founditem.service';
import { WishlistService } from '../../services/wishlist.service';

declare var swal: any;

@Component({
    moduleId: module.id,
    selector: 'user-details',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css'],
    providers: [UsersService, LostItemService, FoundItemService, WishlistService],
 })
export class UserDetailComponent implements OnInit {
    id: number;
    private sub: any;
    user = new User ('', '', '', '',  '');
    errorMessage: String;
    lostItems: LostItem[];
    foundItemsByOwner: FoundItem[];
    wishlistByUser: Wishlist [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private usersService:UsersService,
        private lostItemService: LostItemService,
        private foundItemService: FoundItemService,
        private wishlistService: WishlistService,
      ) {}

        getUserById(){
        this.usersService.getOneUser(this.id)
            .subscribe (user => this.user = user,
                error => this.errorMessage = <any>error);
    }

    getLostItemsByUserId(){
        this.lostItemService.getLostItemsByUserId(this.id).subscribe(
            lostItems => this.lostItems = lostItems,
            error => this.errorMessage = <any>error
        )
    }

    getFoundItemsByOwnerId(){
        this.foundItemService.getFoundItemsByOwnerId(this.id).subscribe(
            foundItemsByOwner => this.foundItemsByOwner = foundItemsByOwner,
            error => this.errorMessage = <any>error
        )
    }

    getWishlistByUserId(){
        this.wishlistService.getWishlistByUserId(this.id).subscribe(
            wishlistByUser => this.wishlistByUser = wishlistByUser,
            error => this.errorMessage = <any>error

        )
    }

    submitForm(myForm:NgForm){
        swal(
            'Good job!',
            'You just saved all changes!',
            'success'
          );
        let update = JSON.stringify(myForm.value);
        this.usersService.updateUser(update);
    }


    deleteThisUser(){
        this.usersService.deleteUser(this.id);     
        // location.reload();   
        swal(
            'Deleted!',
            'You just deleted the user!',
            'info'
          );
    }

    ngOnInit(){
        this.sub = this.route.params.subscribe(params => {this.id = +params['id'];
    })
    this.getUserById();
    this.getLostItemsByUserId();
    this.getFoundItemsByOwnerId();
    this.getWishlistByUserId();
    }
}

