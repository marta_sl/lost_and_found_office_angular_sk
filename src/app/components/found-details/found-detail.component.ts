import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '../../entities/user';
import { FoundItem } from '../../entities/founditem';
import { Wishlist } from '../../entities/wishlist';
import { UsersService } from '../../services/users.service';
import { FoundItemService } from '../../services/founditem.service';
import { WishlistService } from '../../services/wishlist.service';


declare var swal: any;

@Component({
    moduleId: module.id,
    selector: 'found-detail',
    templateUrl: './found-detail.component.html',
    styleUrls: ['./found-detail.component.css'],
    providers: [UsersService, FoundItemService, WishlistService],
 })
export class FoundItemDetailComponent implements OnInit {


    id: number;
    private sub: any;
    errorMessage: String;
    foundItem = new FoundItem('', '', '', 0, '', '', null, null, null, false, null, null);
    users: User[];
    

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private foundItemService: FoundItemService,
        private usersService: UsersService){}

getFoundItemById(){
    this.foundItemService.getOneFoundItem(this.id)
    .subscribe(
        foundItem => this.foundItem = foundItem,
        error => this.errorMessage = <any>error);
    
}

getAllUsers() {
    this.usersService.getAllUsers()
        .subscribe(
        users => this.users = users,
        error => this.errorMessage = <any>error
        );
}

setUser() {
    swal('Good job!',
        'You just updated found item!',
        'success');
        let update = JSON.stringify(this.foundItem);
        this.foundItemService.updateFoundItem(update);
}

changeStatus(){
    this.foundItem.statusReturned=true;
    swal(
        'Good job!',
        'You just changed status to RETURNED!',
        'success'
      );
    let update = JSON.stringify(this.foundItem);
    this.foundItemService.changeStatusFoundItem(update);
}

deleteThisLostItem(){
    this.foundItemService.deleteFoundItem(this.id);
    swal(
        'Deleted!',
        'You just deleted the found item!',
        'info'
      );
    }

    ngOnInit(): void {
        this.sub = this.route.params.subscribe(params => {this.id = +params['id'];
    })
    this.getFoundItemById();
    this.getAllUsers();
    }
}


