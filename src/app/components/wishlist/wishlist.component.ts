import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Wishlist } from '../../entities/wishlist';
import { User } from '../../entities/user';
import { FoundItem } from '../../entities/founditem';
import { WishlistService } from '../../services/wishlist.service'
import { UsersService } from '../../services/users.service';
import { FoundItemService } from '../../services/founditem.service';

declare var swal: any;
declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'my-wishlist',
    templateUrl: './wishlist.component.html',
    styleUrls: ['./wishlist.component.css'],
    providers: [WishlistService, UsersService, FoundItemService],
})

export class WishlistComponent implements OnInit {

    id: number;
    private sub: any;
    errorMessage: String;
    foundItem: FoundItem; //wishlistdisplay
    foundItems: FoundItem; //optional
    wishlistArray: Wishlist [];
    wishlist = new Wishlist (this.foundItem, null, null, 0);
    rankingCount:any;
    users: User[];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private wishlistService: WishlistService,
        private usersService: UsersService,
        private foundItemService: FoundItemService,) { }

        getFoundItemById(){
            this.foundItemService.getOneFoundItem(this.id)
            .subscribe(
                foundItem => this.foundItem = foundItem,
                error => this.errorMessage = <any>error);
               
        }

        getFoundItemsById(){
            this.foundItemService.getOneFoundItem(this.id)
            .subscribe(
                foundItems => this.foundItems = foundItems,
                error => this.errorMessage = <any>error);
            
        }

        getWishlistByItemId(){
           this.wishlistService.getWishlistByItemId(this.id)
            .subscribe(
                wishlistArray => this.wishlistArray = wishlistArray,
                error => this.errorMessage = <any>error,
            );
        }

        countRanking(){
            this.wishlistService.getRankingByItem(this.id).subscribe(
                rankingCount=> this.rankingCount = rankingCount,
                error => this.errorMessage = <any>error,
            );
        }

        deleteWishlist(wishlistDelete: Wishlist){
            this.wishlistService.deleteWishlist(wishlistDelete.id); 
            this.wishlistArray.splice(this.wishlistArray.indexOf(wishlistDelete),1)
            swal(
                'Deleted!',
                'You just deleted the wishlist!',
                'info'
              );
        }

        getUsersAllowed(){
            this.usersService.getUsersWithWishlistAllowed()
            .subscribe(
            users => this.users = users,
            error => this.errorMessage = <any>error
            );
        }

        submitForm(myForm:NgForm){
            swal('Good job!',
            'You just added new wishlist!',
            'success');

        this.wishlistArray.push(myForm.value);
        this.wishlist.foundItem=this.foundItem;

        var lastIndex = this.wishlistArray.length;
        this.wishlist.userInWishlist=this.wishlistArray[lastIndex-1].userInWishlist;
        this.wishlist.ranking=this.wishlistArray[lastIndex-1].ranking;

        let input = JSON.stringify(this.wishlist);
        this.wishlistService.addNewWishlist(input);
        myForm.resetForm();
        jQuery("modal").modal("hide");
        }

    ngOnInit(): void {
        this.sub = this.route.params.subscribe(params => {
        this.id = +params['id'];
        })
        this.getFoundItemById();
        this.getFoundItemsById();
        this.getWishlistByItemId();
        this.countRanking();
        this.getUsersAllowed();

    }

}

