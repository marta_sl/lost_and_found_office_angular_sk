import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';


import { User } from '../../entities/user';
import { FoundItem } from '../../entities/founditem';
import { Wishlist } from '../../entities/wishlist';
import { UsersService } from '../../services/users.service';
import { FoundItemService } from '../../services/founditem.service';
import { WishlistService } from '../../services/wishlist.service';
import { SortFoundService } from '../../services/sortFound.service';


declare var swal: any;
declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'founditem',
    templateUrl: './founditem.component.html',
    styleUrls: ['./founditem.component.css'],
    providers: [UsersService, FoundItemService, WishlistService],
})

export class FoundItemComponent implements OnInit {

    foundItems: FoundItem[];
    errorMessage: String;
    foundItem = new FoundItem('', '', '', 0, '', '', null, null, null, false, null, null);
    users: User[];

    categories = ['Home', 'Garden', 'Clothes', 'Cars', 'Electronic', 'Books', 'Multimedia', 'Pets', 'Other'];


    constructor(
        private router: Router,
        private foundItemService: FoundItemService,
        private usersService: UsersService,
        private sortFoundService: SortFoundService) { }

        getAllFoundItems() {
      this.foundItemService.getAllFoundItems()
      .subscribe(
          foundItems => this.foundItems = foundItems,
          error => this.errorMessage = <any>error
      );
        }
        submitForm(myForm: NgForm) {
            swal('Good job!',
                'You just added found item!',
                'success');
            let input = JSON.stringify(myForm.value);
            this.foundItemService.addNewFoundItem(input);
            this.foundItems.push(myForm.value);
            myForm.resetForm();
            jQuery("modal").modal("hide");
        }

        deleteFoundItem(foundItemDelete: FoundItem){
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete the lost item!'
            }).then(() => {
                this.foundItemService.deleteFoundItem(foundItemDelete.id);
                this.foundItems.splice(this.foundItems.indexOf(foundItemDelete),1)
                swal(
                    'Deleted!',
                    'The item has been deleted.',
                    'success'
                )
            }, function (dismiss: any) {
                if (dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        'Item will not be deleted',
                        'info'
                    )
                }
            })
        }

        sortByName(foundItems: FoundItem[]){
            this.sortFoundService.sortByName(foundItems);
        
           }
            sortByCategory(foundItems: FoundItem[]){
                this.sortFoundService.sortByCategory(foundItems);
                
            }
            sortByInfo(foundItems: FoundItem[]){
        this.sortFoundService.sortByInfo(foundItems);
            }
        
            sortByStatus(foundItems: FoundItem[]){
        this.sortFoundService.sortByStatus(foundItems);
            }


    ngOnInit(): void {
        this.getAllFoundItems();

    }
}
