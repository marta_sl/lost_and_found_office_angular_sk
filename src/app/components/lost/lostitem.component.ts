import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '../../entities/user';
import { LostItem } from '../../entities/lostitem';
import { LostItemService } from '../../services/lostitem.service';
import { UsersService } from '../../services/users.service';
import { SortLostService } from '../../services/sortLost.service';


declare var swal: any;
declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'lostitem',
    templateUrl: './lostitem.component.html',
    styleUrls: ['./lostitem.component.css'],
    providers: [LostItemService],

})


export class LostItemComponent implements OnInit {

    lostItems: LostItem[];
    errorMessage: String;
    lostItem = new LostItem('', '', '', 0, '', '', null, null, false, null);
    users: User[];

    categories = ['Home', 'Garden', 'Clothes', 'Cars', 'Electronic', 'Books', 'Multimedia', 'Pets', 'Other'];

    constructor(
        private router: Router,
        private lostItemService: LostItemService,
        private usersService: UsersService,
    private sortLostService: SortLostService) { }

    getAllLostItems() {
        this.lostItemService.getAllLostItems()
            .subscribe(
            lostItems => this.lostItems = lostItems,
            error => this.errorMessage = <any>error,
        );
    }

    getAllUsers() {
        this.usersService.getAllUsers()
            .subscribe(
            users => this.users = users,
            error => this.errorMessage = <any>error
            );
    }

    submitForm(myForm: NgForm) {
        swal('Good job!',
            'You just added lost item!',
            'success');
        let input = JSON.stringify(myForm.value);
        this.lostItemService.addNewLostItem(input);
        this.lostItems.push(myForm.value);
        myForm.resetForm();
        jQuery("modal").modal("hide");
    }

    deleteLostItem(lostItemDelete: LostItem) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete the lost item!'
        }).then(() => {
            this.lostItemService.deleteLostItem(lostItemDelete.id);
            this.lostItems.splice(this.lostItems.indexOf(lostItemDelete), 1);
            swal(
                'Deleted!',
                'The item has been deleted.',
                'success'
            )
        }, function (dismiss: any) {
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Item will not be deleted',
                    'info'
                )
            }
        })
    }
    sortByName(lostItems: LostItem[]){
    this.sortLostService.sortByName(lostItems);

   }
    sortByCategory(lostItems: LostItem[]){
        this.sortLostService.sortByCategory(lostItems);
        
    }
    sortByInfo(lostItems: LostItem[]){
this.sortLostService.sortByInfo(lostItems);
    }

    sortByStatus(lostItems: LostItem[]){
this.sortLostService.sortByStatus(lostItems);
    }

    ngOnInit(): void {

        this.getAllLostItems();
        this.getAllUsers();
    }
}

