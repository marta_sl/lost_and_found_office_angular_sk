import { NO_ERRORS_SCHEMA, DebugElement, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { async } from '@angular/core/testing';
import "rxjs/Rx";
import { HttpModule} from '@angular/http';

import {Observable} from "rxjs/Observable";
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { LostItemComponent } from './lostitem.component';
import { LostItemService } from '../../services/lostitem.service';
import { SortLostService } from '../../services/sortLost.service';
import { UsersService } from '../../services/users.service';


describe('Test Lost Item Component', () =>{
    let fixture: ComponentFixture<LostItemComponent>;
    let component: LostItemComponent;
    let de: DebugElement;
    let lostItemService: LostItemService;

    beforeEach(async(()=>{
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpModule],
            declarations: [LostItemComponent],
            providers: [
                { provide: Router},
                { provide: LostItemService},
                { provide: SortLostService},
                { provide: UsersService}],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
                NO_ERRORS_SCHEMA
            ],
        })
        .compileComponents();
    }))

beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [],
        declarations: [LostItemComponent],
        providers: [LostItemService, SortLostService, UsersService],
        schemas: [
            CUSTOM_ELEMENTS_SCHEMA,
            NO_ERRORS_SCHEMA
        ],
    })

fixture = TestBed.createComponent(LostItemComponent);
component = fixture.componentInstance;
de = fixture.debugElement;
lostItemService = TestBed.get(LostItemService);

})
it('should call lgetAllLostItems and getAllUsers when init',()=>{
    spyOn(component, 'getAllLostItems').and.callThrough;
    spyOn(component, 'getAllUsers').and.callThrough;
    fixture.detectChanges();
    
    expect(component.getAllLostItems).toHaveBeenCalled;
    expect(component.getAllUsers).toHaveBeenCalled;
})

it('should set lostItems property when init', async(()=>{
    spyOn(component, 'getAllLostItems').and.returnValue(Observable.of('fake lost'));
    spyOn(component, 'getAllUsers').and.callThrough;
    fixture.detectChanges();
  
    expect(component.lostItems).toEqual('fake lost');

}))
})