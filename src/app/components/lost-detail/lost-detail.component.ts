import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '../../entities/user';
import { LostItem } from '../../entities/lostitem';
import { LostItemService } from '../../services/lostitem.service';

declare var swal: any;

@Component({
    moduleId: module.id,
    selector: 'lost-detail',
    templateUrl: './lost-detail.component.html',
    styleUrls: ['./lost-detail.component.css'],
    providers: [LostItemService],
 })
export class LostItemDetailComponent implements OnInit {

    id: number;
    private sub: any;
    errorMessage: String;
    lostItem = new LostItem ('','','',0,'','',null,null,false, null);

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private lostItemService: LostItemService,){}


getLostItemById(){

    this.lostItemService.getOneLostItem(this.id)
    .subscribe(
        lostItem => this.lostItem = lostItem,
        error => this.errorMessage = <any>error);
    }

    deleteThisLostItem(){
        this.lostItemService.deleteLostItem(this.id);
        // location.reload();   
        swal(
            'Deleted!',
            'You just deleted the lost item!',
            'info'
          );
        }

    changeStatus(){
    this.lostItem.foundStatus=true;
    swal(
        'Good job!',
        'You just changed status to FOUND!',
        'success'
      );
    let update = JSON.stringify(this.lostItem);
    this.lostItemService.updateLostItem(update);
        }

    ngOnInit(){
        this.sub = this.route.params.subscribe(params => {this.id = +params['id'];
    })
    this.getLostItemById();
    }
}