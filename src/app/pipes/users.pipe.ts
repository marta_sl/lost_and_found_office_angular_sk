import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { User } from '../entities/user';

@Pipe({ name: 'usersNameFilter' })
@Injectable()
export class UsersPipe implements PipeTransform {

    transform(users: any[], searchText: any): any {
        if(searchText == null || searchText === undefined || searchText==[]) return users;
        return users.filter(users => 
            (users.firstName.toLowerCase().includes(searchText.toLowerCase())) || 
            (users.lastName.toLowerCase().includes(searchText.toLowerCase())) ||
            (users.shortName.toLowerCase().includes(searchText.toLowerCase())));
      }
    }

