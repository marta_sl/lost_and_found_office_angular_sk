import {User} from './user';

export class FoundItem 
{
    id: number;
    name: String;
    category: String;
    color: String;
    weight_gm: number;
    shortInfo: String;
    photoUrl: String;
    dateWhenFound: any;
    owner: User;
    dateWhenOwnerFound: Date;
    statusReturned: boolean;
    dateWhenReturned: Date;
    returnedToUser: User;


    constructor (name: String, category: String, color: String, weight_gm: number,
        shortInfo: String, photoUrl: String,  dateWhenFound: Date,  owner: User,
        dateWhenOwnerFound: Date, statusReturned: boolean, dateWhenReturned: Date, returnedToUser: User){
        this.name = name;
        this.category = category;
        this.color = color;
        this.weight_gm = weight_gm;
        this.shortInfo = shortInfo;
        this.photoUrl = photoUrl;
        this.dateWhenFound = dateWhenFound;
        this.owner = owner;
        this.dateWhenOwnerFound = dateWhenOwnerFound;
        this.statusReturned = statusReturned;
        this.dateWhenReturned = dateWhenReturned;
        this.returnedToUser = returnedToUser;
    }

}



