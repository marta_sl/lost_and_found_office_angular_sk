import {User} from './user';

export class LostItem 
{
    id: number;
    name: String;
    category: String;
    color: String;
    weight_gm: number;
    shortInfo: String;
    photoUrl: String;
    userWhoLost: User;
    dateWhenLost: Date;
    foundStatus: boolean;
    dateWhenFound: Date;


    constructor (name: String, category: String, color: String, weight_gm: number,
        shortInfo: String, photoUrl: String, userWhoLost: User, dateWhenLost: Date, foundStatus:boolean, dateWhenFound: Date ){
        this.name = name;
        this.category = category;
        this.color = color;
        this.weight_gm = weight_gm;
        this.shortInfo = shortInfo;
        this.photoUrl = photoUrl;
        this.userWhoLost = userWhoLost;
        this.dateWhenFound = dateWhenLost;
        this.foundStatus = foundStatus;
        this.dateWhenFound = dateWhenFound;
    }
}