import {User} from './user';
import {FoundItem} from './founditem';

export class Wishlist 
{
    id: number;
    foundItem: FoundItem;
    userInWishlist: User;
    dateWhenAddedToWishlist: Date;
    ranking: number;

    constructor (foundItem: FoundItem, userInWishlist: User, dateWhenAddedToWishlist: Date, ranking: number){
        this.foundItem = foundItem;
        this.userInWishlist = userInWishlist;
        this.dateWhenAddedToWishlist = dateWhenAddedToWishlist;
        this.ranking = ranking;
    }

}