export class User 
{
    id: number;
    firstName: String;
    lastName: String;
    shortName: String;
    email: String;
    phone: String;

    constructor (firstName:String, lastName:String, shortName:String, email:String, phone:String){
        this.firstName = firstName;
        this.lastName = lastName;
        this.shortName = shortName;
        this.email = email;
        this.phone = phone;
    }

}