import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { LostItemComponent } from './components/lost/lostitem.component';
import { LostItemDetailComponent } from './components/lost-detail/lost-detail.component';
import { FoundItemComponent } from './components/found/founditem.component';
import { FoundItemDetailComponent } from './components/found-details/found-detail.component'
import { WishlistComponent } from './components/wishlist/wishlist.component'


import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'lost', component: LostItemComponent },
  { path: 'found', component: FoundItemComponent },
  { path: '', component: HomeComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'users/detail/:id', component: UserDetailComponent },
  { path: 'lost/detail/:id', component: LostItemDetailComponent },
  { path: 'found/detail/:id', component: FoundItemDetailComponent },
  { path: 'wishlist/:id', component: WishlistComponent },



];
export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
