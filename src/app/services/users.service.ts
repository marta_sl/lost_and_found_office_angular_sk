import { Injectable }    from '@angular/core';
import { Http, Headers, RequestOptions }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { User } from '../entities/user';

@Injectable()
export class UsersService {

    constructor(private http: Http) {}

    private usersUrl = 'http://localhost:8080/api/user/all';
    private addUrl = 'http://localhost:8080/api/user/add';
    private getOneUrl = 'http://localhost:8080/api/user/user';
    private updateUrl = 'http://localhost:8080/api/user/update';
    private wishlistAllowedUrl = 'http://localhost:8080/api/user/all/wishlist';

    headers = new Headers({ 'Content-Type': 'application/json' });
    options = new RequestOptions({ headers: this.headers });

    getAllUsers(){
        return this.http.get(this.usersUrl)
        .map(res => <User[]> res.json())
        .catch(this.handleError);
    }

    addNewUser(input: any){
        this.http.post(this.addUrl, input, {headers: this.headers} ).
        subscribe();
    }

    getOneUser(id: number){
       return this.getAllUsers().map((users: User[]) => users
       .find(user => user.id == id)).catch(this.handleError);
    }

    updateUser(update:any){
        this.http.post(this.updateUrl, update, {headers: this.headers}).
        subscribe();
    }

    deleteUser(idDeleted: number){
        this.http.post('http://localhost:8080/api/user/delete'+idDeleted, {headers: this.headers})
        .subscribe();
    }

    getUsersWithWishlistAllowed(){
        return this.http.get(this.wishlistAllowedUrl)
        .map(res => <User[]> res.json())
        .catch(this.handleError);
    }

    private handleError(error: any): Observable<any> {
        console.error('An error occurred', error);
        return Observable.throw(error.message || error);
      } 
}

