import { Injectable }      from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';


declare var Auth0Lock: any;

@Injectable()
export class Auth {
  lock = new Auth0Lock('67fv1Y9nAo_QsK8h0w5RETTcEvJpU1t8', 'martasl.eu.auth0.com',{
     allowSignUp: false
  });

  constructor() {
    this.lock.on("authenticated", (authResult:any) => {
      this.lock.getProfile(authResult.idToken, function(error:any, profile:any){
          if(error){
              throw new Error(error);
          }
            localStorage.setItem('id_token', authResult.idToken);
            localStorage.setItem('profile', JSON.stringify(profile)); 
      });
    });
  }

  public login() {
    this.lock.show();
  };

  public authenticated() {
    return tokenNotExpired('id_token');
  };

  public logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
  };
}