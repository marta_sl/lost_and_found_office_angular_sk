import { Injectable }    from '@angular/core';
import { Http, Headers, RequestOptions }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { LostItem } from '../entities/lostitem';

@Injectable()
export class LostItemService {

    constructor(private http: Http) {}

    private lostItemsUrl = 'http://localhost:8080/api/lost/all';
    private addLostItemUrl = 'http://localhost:8080/api/lost/add';
    private updatedLostItemUrl = 'http://localhost:8080/api/lost/update';

    headers = new Headers({ 'Content-Type': 'application/json' });
    options = new RequestOptions({ headers: this.headers });

getAllLostItems(){
    return this.http.get(this.lostItemsUrl).map(res => <LostItem[]> res.json())
    .catch(this.handleError);
}

getLostItemsByUserId(userId:number){
    return this.http.get('http://localhost:8080/api/lost/user'+userId).map(res => <LostItem[]> res.json())
    .catch(this.handleError);
}

addNewLostItem(input: any){
this.http.post(this.addLostItemUrl, input, {headers: this.headers}).
subscribe();
}

getOneLostItem(id: number){
    return this.getAllLostItems().map((lostItemsList: LostItem[]) => lostItemsList
    .find(lostItem => lostItem.id == id)).catch(this.handleError);

    }

updateLostItem(updated:any){
    this.http.post(this.updatedLostItemUrl, updated,{headers: this.headers} )
    .subscribe();
}

deleteLostItem(idDeletedLostItem:number){
    this.http.post('http://localhost:8080/api/lost/delete'+idDeletedLostItem, {headers: this.headers})
    .subscribe();
}


private handleError(error: any): Observable<any> {
        console.error('An error occurred', error);
        return Observable.throw(error.message || error);
      } 
}

