import { Injectable }    from '@angular/core';
import { Http, Headers, RequestOptions }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Wishlist } from '../entities/wishlist';

@Injectable()
export class WishlistService {
    listOfWhislist: Wishlist [];

    constructor(private http: Http) {}

    private addWishlistUrl = 'http://localhost:8080/api/wishlist/add';
    
        headers = new Headers({ 'Content-Type': 'application/json' });
        options = new RequestOptions({ headers: this.headers });

    

getWishlistByUserId (userId: number){
    return this.http.get('http://localhost:8080/api/wishlist/user'+userId).map(res => <Wishlist[]> res.json())
    .catch(this.handleError);
}

getWishlistByItemId (itemId: number){
    return this.http.get('http://localhost:8080/api/wishlist/item'+itemId).map(res => <Wishlist[]> res.json())
    .catch(this.handleError);
}

addNewWishlist(input: any) {
    this.http.post(this.addWishlistUrl, input, {headers: this.headers} ).
    subscribe();
}

deleteWishlist(idDeleted: number){
    this.http.post('http://localhost:8080/api/wishlist/delete'+idDeleted, {headers: this.headers})
    .subscribe();
}

getRankingByItem (itemId: number){
    return this.http.get('http://localhost:8080/api/wishlist/ranking'+itemId).map(res => <number> res.json())
    .catch(this.handleError);
}

getUserWishlistCount (userId: number){
    return this.http.get('http://localhost:8080/api/wishlist/usercount'+userId).catch(this.handleError);
}


private handleError(error: any): Observable<any> {
        console.error('An error occurred', error);
        return Observable.throw(error.message || error);
      } 
}

