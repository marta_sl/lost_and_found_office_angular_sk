import { Injectable }    from '@angular/core';

import { LostItem } from '../entities/lostitem';

@Injectable()
export class SortLostService {

    sortByName(lostItems: LostItem[]){
        if(lostItems[0].name.toLowerCase() > lostItems[1].name.toLowerCase()){
            lostItems.sort((i1, i2) => {
                return i1.name.toLowerCase() < i2.name.toLowerCase() ? -1 :1;
            })
        } else{
            lostItems.sort((i1, i2) => {
                return i1.name.toLowerCase() > i2.name.toLowerCase() ? -1 :1;
            })
        }

   }
    sortByCategory(lostItems: LostItem[]){
        if(lostItems[0].category > lostItems[1].category){
            lostItems.sort((i1, i2) => {
                return i1.category < i2.category ? -1 :1;
            })
        }else{
            lostItems.sort((i1, i2) => {
                return i1.category > i2.category ? -1 :1;
            })
        }
        
    }
    sortByInfo(lostItems: LostItem[]){
        if(lostItems[0].shortInfo > lostItems[1].shortInfo){
            lostItems.sort((i1, i2) => {
                return i1.shortInfo.toLowerCase() < i2.shortInfo.toLowerCase() ? -1 :1;
            })
        }else{
            lostItems.sort((i1, i2) => {
                return i1.shortInfo.toLowerCase() > i2.shortInfo.toLowerCase() ? -1 :1;
            })
        }
    }

    sortByStatus(lostItems: LostItem[]){
        if(lostItems[0].foundStatus.valueOf().toString() > lostItems[1].foundStatus.valueOf().toString()){
            lostItems.sort((i1, i2) => {
                return i1.foundStatus.valueOf().toString() < i2.foundStatus.valueOf().toString() ? -1 :1;
            })
        } else{
            lostItems.sort((i1, i2) => {
                return i1.foundStatus.valueOf().toString() > i2.foundStatus.valueOf().toString() ? -1 :1;
            })
        }

    }

}