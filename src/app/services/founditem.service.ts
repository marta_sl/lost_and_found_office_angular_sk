import { Injectable }    from '@angular/core';
import { Http, Headers, RequestOptions }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { FoundItem } from '../entities/founditem';

@Injectable()
export class FoundItemService {

    constructor(private http: Http) {}

    private allFoundItemsUrl = 'http://localhost:8080/api/found/all';
    private addFoundItemUrl = 'http://localhost:8080/api/found/add';
    private updateFoundItemUrl = 'http://localhost:8080/api/found/update';
    private changeStatusFoundItemUrl = 'http://localhost:8080/api/found/status';
    
        headers = new Headers({ 'Content-Type': 'application/json' });
        options = new RequestOptions({ headers: this.headers });


getFoundItemsByOwnerId(ownerId:number){
    return this.http.get('http://localhost:8080/api/found/user'+ownerId).map(res => <FoundItem[]> res.json())
    .catch(this.handleError);
}

getAllFoundItems(){
    return this.http.get(this.allFoundItemsUrl).map(res => <FoundItem[]> res.json())
    .catch(this.handleError);
}

addNewFoundItem(input: any){
    this.http.post(this.addFoundItemUrl, input, {headers: this.headers}).
    subscribe();
}

getOneFoundItem (id: number){
    return this.getAllFoundItems().map((foundItems: FoundItem[])=> foundItems
.find(foundItem => foundItem.id == id)).catch(this.handleError);
}

updateFoundItem(updated:any){
    this.http.post(this.updateFoundItemUrl, updated, {headers: this.headers} )
    .subscribe();
}

changeStatusFoundItem(updated:any){
    this.http.post(this.changeStatusFoundItemUrl, updated, {headers: this.headers} )
    .subscribe();
}

deleteFoundItem(idFoundItem: number){
    this.http.post('http://localhost:8080/api/found/delete'+idFoundItem, {headers: this.headers})
    .subscribe();
}

    private handleError(error: any): Observable<any> {
        console.error('An error occurred', error);
        return Observable.throw(error.message || error);
      } 
}


