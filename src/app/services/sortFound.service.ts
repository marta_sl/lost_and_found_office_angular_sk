import { Injectable }    from '@angular/core';

import { FoundItem } from '../entities/founditem';

@Injectable()
export class SortFoundService {

    sortByName(foundItems: FoundItem[]){
        if(foundItems[0].name >= foundItems[1].name){
            foundItems.sort((i1, i2) => {
                return i1.name.toLowerCase() < i2.name.toLowerCase() ? -1 :1;
            })
        } else{
            foundItems.sort((i1, i2) => {
                return i1.name.toLowerCase() > i2.name.toLowerCase() ? -1 :1;
            })
        }

   }
    sortByCategory(foundItems: FoundItem[]){
        if(foundItems[0].category > foundItems[1].category){
            foundItems.sort((i1, i2) => {
                return i1.category < i2.category ? -1 :1;
            })
        }else{
            foundItems.sort((i1, i2) => {
                return i1.category > i2.category ? -1 :1;
            })
        }
        
    }
    sortByInfo(foundItems: FoundItem[]){
        if(foundItems[0].shortInfo > foundItems[1].shortInfo){
            foundItems.sort((i1, i2) => {
                return i1.shortInfo.toLowerCase() < i2.shortInfo.toLowerCase() ? -1 :1;
            })
        }else{
            foundItems.sort((i1, i2) => {
                return i1.shortInfo.toLowerCase() > i2.shortInfo.toLowerCase() ? -1 :1;
            })
        }
    }

    sortByStatus(foundItems: FoundItem[]){
        if(foundItems[0].statusReturned.valueOf().toString() > foundItems[1].statusReturned.valueOf().toString()){
            foundItems.sort((i1, i2) => {
                return i1.statusReturned.valueOf().toString() < i2.statusReturned.valueOf().toString() ? -1 :1;
            })
        } else{
            foundItems.sort((i1, i2) => {
                return i1.statusReturned.valueOf().toString() > i2.statusReturned.valueOf().toString() ? -1 :1;
            })
        }

    }

}