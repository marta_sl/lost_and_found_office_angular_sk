import { NgModule ,  CUSTOM_ELEMENTS_SCHEMA,  NO_ERRORS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule, Http}    from '@angular/http';
import { AUTH_PROVIDERS } from 'angular2-jwt';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { RatingModule } from 'ng2-rating';
import {DataTableModule} from "angular2-datatable";

import { routing, appRoutingProviders, AppRoutingModule } from './app-routing.module';
import { RouterModule } from "@angular/router";

//components
import { AppComponent }         from './app.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { LostItemComponent } from './components/lost/lostitem.component';
import { LostItemDetailComponent } from './components/lost-detail/lost-detail.component'
import { FoundItemComponent } from './components/found/founditem.component'
import { FoundItemDetailComponent } from './components/found-details/found-detail.component'
import { WishlistComponent } from './components/wishlist/wishlist.component'

//services
import { FoundItemService } from './services/founditem.service';
import { LostItemService } from './services/lostitem.service';
import { UsersService } from './services/users.service';
import { WishlistService } from './services/wishlist.service';
import { SortLostService } from './services/sortLost.service';
import { SortFoundService } from './services/sortFound.service';
//pipes
import { UsersPipe } from './pipes/users.pipe';
//auth
import {Auth} from './services/auth.service';
import {AuthGuard} from './auth.guard';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    routing,
    RouterModule,
    Ng2Bs3ModalModule,
    RatingModule,
    DataTableModule,

  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    UsersComponent,
    UserDetailComponent,
    LostItemComponent,
    LostItemDetailComponent,
    FoundItemComponent,
    FoundItemDetailComponent,
    WishlistComponent,
    UsersPipe,
    
  ],
  providers: [ 
    UsersService,
    LostItemService,
    FoundItemService,
    WishlistService,
    SortLostService,
    SortFoundService,
    appRoutingProviders,
    AUTH_PROVIDERS,
    Auth,
    AuthGuard,
  ],
  bootstrap: [ AppComponent ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
],
})
export class AppModule { }
